/*
 ==============================================================================
 Name        : aes_testvectors_POLY1305.h
 Author      : polfosol
 Version     : 1.2.0.0
 Copyright   : copyright © 2024 - polfosol
 Description : checking the test vectors for AES-POLY1305
 ==============================================================================
 */

#include "aes_testvectors.h"

#if defined(POLY_TEST_FILE) && !defined(_TESTING_POLY1305_H_)
#define _TESTING_POLY1305_H_

int verifypoly(uint8_t* key, uint8_t* non, uint8_t* d, uint8_t* m,
                      size_t nd, char* r)
{
    char sk[2 * AES_KEY_SIZE + 33], smac[33], msg[30];
    uint8_t tmp[16], v = 0;
    strcpy(msg, "passed the test");
    AES_Poly1305(key, non, d, nd, tmp);

    if ((v = memcmp(m, tmp, 16)) != 0)  strcpy(msg, "failed");

    bytes2str(key, sk, AES_KEY_SIZE + 16);
    bytes2str(m, smac, 16);
    sprintf(r, "%s\nK: %s\npoly: %s\n", msg, sk, smac);
    return v;
}

void aes_poly1305_test(FILE** files, unsigned* count)
{
    const char *head[] = POLY_HEADLINES;
    char buffer[0x20100], *value = NULL;
    size_t s[4] = { 0 };
    uint8_t j, e = 0, key[AES_KEY_SIZE + 16], nc[16], d[0x10100], m[16];

    while (fgets(buffer, sizeof buffer, *files) != NULL)
    {
        buffer[strcspn(buffer, "\n")] = 0;
        j = strlen(buffer) < 4 ? 4 : 0;

        for (; j < 4 && strncmp(buffer, head[j], strlen(head[j])); ++j);

        if (j != 4)
        {
            value = strrchr(buffer, ' ') + 1;
            s[j] = strlen(value) / 2;
            e += (j == 2 || j == 3);
        }
        switch (j)
        {
        case 0:
            if (s[0] == sizeof key) str2bytes(value, key);
            break;
        case 1:
            str2bytes(value, nc);
            break;
        case 2:
            str2bytes(value, d);
            break;
        case 3:
            str2bytes(value, m);
            break;
        }
        if (e == 2)
        {
            if (s[0] == AES_KEY_SIZE + 16)
            {
                e = verifypoly(key, nc, d, m, s[2], buffer);
                fprintf(files[2 - !e], "%s\n", buffer); /* save the log */
                ++count[0];
                count[1] += e != 0;
            }
            e = 0;
        }
    }
    count[2] = ~0U;
}

#endif /* header guard */
