/*
 ==============================================================================
 Name        : aes_testvectors_XTS.h
 Author      : polfosol
 Version     : 1.2.0.0
 Copyright   : copyright © 2024 - polfosol
 Description : checking the test vectors for AES-XTS
 ==============================================================================
 */

#include "aes_testvectors.h"

#if defined(XTS_TEST_FILE) && !defined(_TESTING_XTS_H_)
#define _TESTING_XTS_H_

int verifyxts(uint8_t* key, uint8_t* i, uint8_t* p, uint8_t* c,
                     size_t np, char* r)
{
    char sk[4 * AES_KEY_SIZE + 1], si[33], sp[0x80], sc[0x80], msg[30];
    uint8_t tmp[0x80], v = 0;
    strcpy(msg, "passed the test");

    AES_XTS_encrypt(key, i, p, np, tmp);
    if (memcmp(c, tmp, np))
    {
        strcpy(msg, "encrypt failure");
        v = 1;
    }
    memset(tmp, 0xcc, sizeof tmp);
    AES_XTS_decrypt(key, i, c, np, tmp);
    if (memcmp(p, tmp, np))
    {
        strcat(strcpy(msg, v ? "encrypt & " : ""), "decrypt failure");
        v |= 2;
    }
    bytes2str(key, sk, 2 * AES_KEY_SIZE);
    bytes2str(i, si, 16);
    bytes2str(p, sp, np);
    bytes2str(c, sc, np);
    sprintf(r, "%s\nK: %s\ni: %s\nP: %s\nC: %s", msg, sk, si, sp, sc);
    return v;
}

void aes_xts_test(FILE** files, unsigned* count)
{
    const char *head[] = XTS_HEADLINES;
    char buffer[0x800], *value = NULL;
    size_t s[5] = { 0 };
    uint8_t j, e = 0, key[2 * AES_KEY_SIZE], iv[16], p[0x80], c[0x80];

    while (fgets(buffer, sizeof buffer, *files) != NULL)
    {
        buffer[strcspn(buffer, "\n")] = 0;
        j = strlen(buffer) < 4 ? 5 : 0;

        for (; j < 5 && strncmp(buffer, head[j], strlen(head[j])); ++j);

        if (j != 5)
        {
            value = strrchr(buffer, ' ') + 1;
            s[j] = j < 4 ? strlen(value) / 2 : 0;
            e += (j == 2 || j == 3);
        }
        switch (j)
        {
        case 0:
            if (s[0] == sizeof key) str2bytes(value, key);
            break;
        case 1:
            str2bytes(value, iv);
            break;
        case 2:
            str2bytes(value, p);
            break;
        case 3:
            str2bytes(value, c);
            break;
        case 4:
            do
                s[4] = s[4] * 10 + *value - '0';
            while (*++value);
            break;
        }
        if (e == 2)
        {
            if (s[0] == 2 * AES_KEY_SIZE && s[4] % 8 == 0)
            {
                e = verifyxts(key, iv, p, c, s[2], buffer);
                fprintf(files[2 - !e], "%s\n", buffer); /* save the log */
                ++count[0];
                if (e & 1) ++count[1];
                if (e & 2) ++count[2];
            }
            e = 0;
        }
    }
}

#endif /* header guard */
